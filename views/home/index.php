<div class='content'>
	<section class='home_page'>
        <div class='move_slide slide-left'><</div><div class='move_slide slide-right'>></div>
		<div class='slider' id="slider_area">
			<div class='slide active_slide'>
				<div class='text_box'>
					<p></p>
					<p class='large_txt'></p>
                </div>
			</div>
        </div>
		
    </section>
    <script>

        function init(){
            db.collection('banner')
            .orderBy('display_order','asc')
            .orderBy('visible','asc')
            .onSnapshot(function(snapshot){
                if(snapshot.docs.length){
                    var active = true;
                    $('#slider_area').empty();
                    snapshot.docs.forEach(function(doc){
                        var data = doc.data();
                        if(!data.visible){
                            return;
                        }
                        var slide = $('<div class="slide" data-time="'+data.display_time+'">');
                        if(active) {
                            slide.addClass('active_slide');
                            active = false;
                        }
                        var text_box = $('<div>').addClass('text_box');
                        if ( data.title ) text_box.append($('<p>').text(data.title));
                        if ( data.subtitle ) text_box.append($('<p>').addClass('large_txt').text(data.subtitle));
                        slide.append(text_box);
                        if(data.banner_type == 1){
                            slide.append('<img src='+data.url+'>');
                        } else {
                            var vid = $('<video oncanplay="this.muted=true">').addClass('video')
                            .attr('type','video/mp4')
                            .attr('src',data.url)
                            .attr('muted','muted')
                            .attr('autoplay','autoplay')
                            .attr('loop','loop');
                            vid[0].load();
                            vid[0].muted = true;
                            vid[0].play();
                            slide.append(vid);
                        }                
                        $('#slider_area').append(slide);
                    });
                }
            });

        }
    </script>
</div>
