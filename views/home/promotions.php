<div class='content'>
	<section class='promotions_page'>
		<section class='inner_content'>
			<div class='img_holder'>
				<img class='scroll' src="<?= FRONT_ASSETS ?>img/scroll.png">
				<embed src="<?= FRONT_ASSETS ?>img/promotions.pdf#toolbar=0" type="application/pdf"></embed>
			</div>
		</section>
	</section>
	<script>
		function init(){
			db.collection('content')
			 .where('page','==','5')
			 .where('visible','==','true')
			 .orderBy('display_order', 'asc')
             .onSnapshot(function(snapshot){

				var embed = $('.inner_content embed');
				if(snapshot.docs.length){
                
                    snapshot.docs.forEach(function(doc){
						var data = doc.data();

						if(data.url.endsWith('.pdf')){
							embed.attr('src',data.url+'#toolbar=0');
						}
					});
				}
			});
		}
	</script>
</div>