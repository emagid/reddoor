<div class='content'>
	<section class='culture_page'>
		<div class='outer_content' style="background-image: url('<?= FRONT_ASSETS ?>img/culture2.jpg');">
			<!-- <video class='video' type='video/mp4' src='<?= FRONT_ASSETS ?>img/culture.mp4' muted autoplay loop></video> -->
			<div class='options'>
				<button id='core' data-btntitle='Core Values' data-currpage='Get in the Know' data-pagedestination='Core Values' class='trackable button white'>CORE VALUES</button>
				<button id='hr' data-btntitle='Human Resources' data-currpage='Get in the Know' data-pagedestination='Human Resources' class='trackable button white'>Human Resources</button>
				<button id='wrd' data-btntitle='Working at the Red Door' data-currpage='Get In The Know' data-pagedestination='Working at the Red Door' class='button trackable white'>Working at the Red Door</button>
				<button id='know' data-btntitle='Whats happening' data-currpage='Get In The Know' data-pagedestination='Whats happening' class='trackable button white'>Whats happening</button>
			</div>
		</div>

		<section class='inner_content core'>
			<p class='back'>HOME</p>
			<img class='scroll' src="<?= FRONT_ASSETS ?>img/scroll.png">
			<div class='img_holder'>
				<img src="<?= FRONT_ASSETS ?>img/core_values.jpg">
				<img src="<?= FRONT_ASSETS ?>img/vision_mission.jpg">
			</div>
		</section>

		<section class='inner_content hr three_choices'>
			<p class='back'>HOME</p>
			<div class='img_choices'>
				<div id='glassdoor' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/glassdoor.jpg');"><p>Glassdoor</p></div>
				<div id='class_pass' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/classpass.jpg');"><p>Class Pass</p></div>
				<div id='red10' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/red10.jpg');"><p>Red10</p></div>
			</div>
			<div class='img_holder glassdoor'>
				<p class='back back_btn'>BACK</p>
				<img src="<?= FRONT_ASSETS ?>img/theglassdoor.jpg">
			</div>
			<div class='img_holder class_pass pdf_holder'>
				<p class='back back_btn'>BACK</p>
				<img class='scroll white' src='<?= FRONT_ASSETS ?>img/scroll.png'>
				<embed src="<?= FRONT_ASSETS ?>img/classPass.pdf#toolbar=0" type="application/pdf"></embed>
			</div>
			<div class='img_holder red10'>
				<p class='back back_btn'>BACK</p>
				<img src="<?= FRONT_ASSETS ?>img/thered10.jpg">
			</div>
		</section>

		<section class='inner_content wrd'>
			<p class='back'>HOME</p>
			<div class='vids'>
				<p class='close'>x</p>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid1.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/vid1.mp4' ></video>
					<p>Promotional Video</p>
				</div>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid2.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mov' src='<?= FRONT_ASSETS ?>img/vid2.mov' ></video>
					<p>Summer 2017 Vivienne Tam</p>
				</div>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid3.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/vid3.mp4' ></video>
					<p>Blake Massage</p>
				</div>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid4.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/vid4.mp4' ></video>
					<p>Red Door Spa Nail Job</p>
				</div>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid5.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/vid5.mp4' ></video>
					<p>Testimonials</p>
				</div>
				<div class='vid' style="background-image: url('<?= FRONT_ASSETS ?>img/vid6.png');">
					<img src="<?= FRONT_ASSETS ?>img/play.png">
					<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/vid6.mp4' muted></video>
					<p>Training Focus</p>
				</div>
			</div>
		</section>

		<section class='inner_content know three_choices'>
			<p class='back'>HOME</p>
			<div class='img_choices'>
				<div id='nps' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/nps.jpg');"><p>NPS</p></div>
				<div id='cup' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/cupping.jpg');"><p>Cupping</p></div>
				<div id='bday' class='img_choice' style="background-image: url('<?= FRONT_ASSETS ?>img/bday.jpg');"><p>Birthdays</p></div>
			</div>
			<div class='img_holder nps'>
				<p class='back back_btn'>BACK</p>
				<embed src="<?= FRONT_ASSETS ?>img/nps.pdf#toolbar=0" type="application/pdf"></embed>
			</div>
			<div class='img_holder cup'>
				<p class='back back_btn'>BACK</p>
				<video type='video/mp4' src='<?= FRONT_ASSETS ?>img/Cupping.mp4' loop></video>
			</div>
			<div class='img_holder bday'>
				<p class='back back_btn'>BACK</p>
				<img src="<?= FRONT_ASSETS ?>img/birthday.jpg">
			</div>
		</section>
			
	</section>
	<script>
		function init(){
			setListeners();

			db.collection('content') //Core Content
			 .where('page','==','1')
			 .where('visible','==','true')
			 .orderBy('display_order', 'asc')
             .onSnapshot(function(snapshot){
				var holder = $('.inner_content.core .img_holder');
				if(snapshot.docs.length){
					holder.empty();
                    snapshot.docs.forEach(function(doc){
						var data = doc.data();
						holder.append( $("<img src='"+data.url+"'/>"));
					});
				}
			});

			db.collection('content') //Hr content
			 .where('page','==','2')
			 .where('visible','==','true')
			 .orderBy('display_order', 'asc')
             .onSnapshot(function(snapshot){

				var thumbnails = $('.inner_content.hr.three_choices .img_choices');
				var holder =    $("<div class='img_holder'>").html("<p class='back back_btn'>BACK</p>");
				
				var pdfHolder = holder.clone().addClass('pdf_holder')
				 .append($("<img class='scroll white' src='<?= FRONT_ASSETS ?>img/scroll.png'>"))
				 .append($("<embed type='application/pdf'>"));
				
				var imgHolder = holder.clone().append($('<img>')); 
				var movHolder = holder.clone().append($("<video type='video/mp4' loop>"));
				if(snapshot.docs.length){
					thumbnails.empty();
					thumbnails.siblings('.img_holder').remove();
					if ( snapshot.docs.length != 3 && snapshot.docs.length < 4 ) {
						$(thumbnails).removeClass('three_choices');
						$(thumbnails).addClass('two_choices');
					}else if ( snapshot.docs.length > 3 ) {
						$(thumbnails).removeClass('three_choices');
						$(thumbnails).addClass('four_choices');
					}
                
                    snapshot.docs.forEach(function(doc){
						var data = doc.data();

						thumbnails.append('<div id="'+doc.id+'" data-btntitle="'+data.title+ '" data-currpage="'+data.page+'" data-pagedestination="'+data.title+'" class="trackable img_choice" style="background-image: url(\''+data.thumbnail+'\')"></div>');
						var el;
						if ( data.title ) {
							$('#'+doc.id).append('<p>'+data.title+'</p>');
						}
						if(data.url.endsWith('.pdf')){
							el = pdfHolder.clone();
							el.find('embed').attr('src',data.url+'#toolbar=0');
						} else if(data.url.endsWith('.mp4')){
							el = movHolder.clone();
							el.find('video').attr('src',data.url);
						} else {
							el = imgHolder.clone();
							el.find('img').attr('src',data.url);
						}
						el.addClass(doc.id);
						thumbnails.parent().append(el);
					});
				}
			});

			db.collection('content') //Working at the Red Door Content 
			 .where('page','==','3')
			 .where('visible','==','true')
			 .orderBy('display_order', 'asc')
			 .onSnapshot(function(snapshot){

				var thumbnails = $('.inner_content.wrd .vids');
				var holder =    $("<div class='vid trackable'> data-btntitle='' data-currpage='' data-pagedestination=''")
				.append("<img src='<?= FRONT_ASSETS ?>img/play.png' />")
				.append("<video type='video/mp4' muted />")
				.append("<p>");

				if(snapshot.docs.length){
					thumbnails.find('.vid').remove();
					thumbnails.siblings('.img_holder').remove();
				
					snapshot.docs.forEach(function(doc){
						var data = doc.data();
						var el = holder.clone();
						el.css('background-image',"url('"+data.thumbnail);
						el.find('video').attr('src',data.url)
						el.find('video').attr('data-btntitle',data.title)
						el.find('video').attr('data-currpage',data.title)
						el.find('video').attr('data-pagedestination',data.title)
						el.find('p').text(data.title)
						
						thumbnails.append(el);
					});
				}
			});

			db.collection('content') //Get in the know content
			 .where('page','==','4')
			 .where('visible','==','true')
			 .orderBy('display_order', 'asc')
			 .onSnapshot(function(snapshot){

				var thumbnails = $('.inner_content.know.three_choices .img_choices');
				var holder =    $("<div class='img_holder'>").html("<p class='back back_btn'>BACK</p>");
				
				var pdfHolder = holder.clone().addClass('pdf_holder')
				.append($("<img class='scroll white' src='<?= FRONT_ASSETS ?>img/scroll.png'>"))
				.append($("<embed type='application/pdf'>"));
				
				var imgHolder = holder.clone().append($('<img>')); 
				var movHolder = holder.clone().append($("<video type='video/mp4' loop>"));
				if(snapshot.docs.length){
					thumbnails.empty();
					thumbnails.siblings('.img_holder').remove();
					if ( snapshot.docs.length != 3 && snapshot.docs.length < 4 ) {
						$(thumbnails).removeClass('three_choices');
						$(thumbnails).addClass('two_choices');
					}else if ( snapshot.docs.length > 3 ) {
						$(thumbnails).removeClass('three_choices');
						$(thumbnails).addClass('four_choices');
					}
				
					snapshot.docs.forEach(function(doc){
						var data = doc.data();

						thumbnails.append('<div id="'+doc.id+'" data-btntitle="'+data.title+ '" data-currpage="'+data.page+'" data-pagedestination="'+data.title+'" class="img_choice" style="background-image: url(\''+data.thumbnail+'\')">');
						var el;
						if ( data.title ) {
							$('#'+doc.id).append('<p>'+data.title+'</p>');
						}
						if(data.url.endsWith('.pdf')){
							el = pdfHolder.clone();
							el.find('embed').attr('src',data.url+'#toolbar=0');
						} else if(data.url.endsWith('.mp4')){
							el = movHolder.clone();
							el.find('video').attr('src',data.url);
						} else {
							el = imgHolder.clone();
							el.find('img').attr('src',data.url);
						}
						el.addClass(doc.id);
						thumbnails.parent().append(el);
					});
				}
			});

		}

		function setListeners(){
			$(document).on('click', '.options .button', function(){
				var id = $(this).attr('id');
				var timer;

				$('.outer_content').fadeOut();
				timer = setTimeout(function(){
					$('.' + id).fadeIn();
					if ( !$('.' + id).hasClass('three_choices') ) {
						$('.' + id).children('.img_holder').slideDown(500);
						$('.' + id).children('.img_holder').css('display', 'flex');
					}
				}, 500);
			});

			$(document).on('click', '.faq_holder', function(){
				var icon = $(this).children('.switch').children('p');

				if ( $(icon).html() == "+" ) {
					$('.switch p').html('+');
					$(icon).html('-');
					$('.faq_a').slideUp();
					$(this).children('.faq_a').slideDown();
				}else {
					$(icon).html('+');
					$(this).children('.faq_a').slideUp();
				}
			});

			$(document).on('click', '.inner_content .back', function(){
				if ( $(this).hasClass('back_btn') ) {
					$('.img_holder').slideUp(500);
					if (  $('.img_holder').children('video')[0] ) {
					$('.img_holder').children('video')[0].pause()
					$('.img_holder').children('video')[0].currentTime = 0;

					}
				}else {
					$('.outer_content').fadeIn();
					$('.inner_content').fadeOut();
					$('.img_holder').slideUp(500);
				}
				console.log('hi')
			});            

			$(document).on('click', '.img_choice', function(){
				var id = $(this).attr('id');
				var self = this;
				var timer;

				$(self).css('transform', 'scale(.9)');
				timer = setTimeout(function(){
					$(self).css('transform', 'scale(1)');
					$('.' + id).slideDown(500);
					$('.' + id).css('display', 'flex');
					var vid = $('.' + id).children('video')[0];
					if (  vid ) {
						$('.' + id).children('video')[0].play();
					}
				}, 200);

			});

			$(document).on('click', '.vid', function(){
				vid = $(this).children('video');

				if ( !$(vid).hasClass('active_vid') ) {
					$(vid).addClass('active_vid').slideDown(500);
					$(vid)[0].play();
					$('.close').fadeIn();
				}

				$(vid).on('ended', function(){
					closeVideo(this);
				});
			});

			$(document).on('click', '.close', function(){
				closeVideo(this);
			});
		}

		function closeVideo( currVid ) {
			vid = $('.active_vid');
			$(vid).removeClass('active_vid').slideUp(500);
			$(vid)[0].pause();
			$(vid)[0].currentTime = 0;
			$('.close').fadeOut(500);
		}
	</script>
</div>