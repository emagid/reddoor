<div class='content'>
	<section class="trivia_page" >
      <!-- Header -->
      <div class='background' style='z-index: -1;'></div>

    <!-- trivia questions -->
      <section class="trivia_content trivia">
        <div class="title_holder">
          <p id="question_counter"><span id='curr_quest'>1</span> / <span id='quest_num'>5</span></p>
          <input type='hidden' id='survey_answer' />
        </div>

        <!-- question 1 -->
        <div class="question_holder active_quest" data-question="QUESTION_ID HERE">
            <div class="question">
                <p><span>1</span>. Please provide a star rating (5 is the highest) - Do you like the TV Screen and its overall look?  (i.e. useful content, effective delivery, quality materials).</p>
            </div>

            <div class="answer click_action">
                <p><i class="far fa-star" data-num='1'></i><span>Sorry, not very effective</span></p>
                <p><i class="far fa-star" data-num='2'></i><span>It's ok, not so much</span></p>
                <p><i class="far fa-star" data-num='3'></i><span>'bout Average</span></p>
                <p><i class="far fa-star" data-num='4'></i><span>Not too shabby</span></p>
                <p><i class="far fa-star" data-num='5'></i><span>Totally cool</span></p>
            </div>
        </div>
        <div class="question_holder" >
            <div class="question">
                <p><span>2</span>. Please provide a star rating (5 is the highest) on do you feel the content displayed is informative (i.e. useful content, effective delivery, quality materials).</p>
            </div>

            <div class="answer click_action">
                <p><i class="far fa-star" data-num='1'></i><span>Sorry, not very effective</span></p>
                <p><i class="far fa-star" data-num='2'></i><span>It's ok, not so much</span></p>
                <p><i class="far fa-star" data-num='3'></i><span>'bout Average</span></p>
                <p><i class="far fa-star" data-num='4'></i><span>Not too shabby</span></p>
                <p><i class="far fa-star" data-num='5'></i><span>Totally cool</span></p>
            </div>
        </div>
        <div class="question_holder" >
            <div class="question">
                <p><span>3</span>. Please provide a star rating (5 is the highest) do you think this tool (TV Screen) will help to enhance your engagement in the workplace?</p>
            </div>

            <div class="answer click_action">
                <p><i class="far fa-star" data-num='1'></i><span>Sorry, not very effective</span></p>
                <p><i class="far fa-star" data-num='2'></i><span>It's ok, not so much</span></p>
                <p><i class="far fa-star" data-num='3'></i><span>'bout Average</span></p>
                <p><i class="far fa-star" data-num='4'></i><span>Not too shabby</span></p>
                <p><i class="far fa-star" data-num='5'></i><span>Totally cool</span></p>
            </div>
        </div>
        <div class="question_holder" >
            <div class="question">
                <p><span>4</span>. What kind of content would you like to see?</p>
            </div>

            <textarea placeholder='Optional comments'></textarea>
            <button class='button white'>NEXT</button>
        </div>
        <div class="question_holder" >
            <div class="question">
                <p><span>5</span>. We went For the Gold, please comment on anything you particularly liked seeing on the screen.</p>
            </div>

            <textarea placeholder='Optional comments'></textarea>
            <button class='button white'>NEXT</button>
        </div>
        <div class="success" >
            <div class="question">
                <p>Thank you!!</p>
            </div>
        </div>
      </section>
  </section>
  <script>  
    var curr_question_id;
    function init(){
        set_listeners();
        db.collection('question')
        .orderBy('display_order','asc')
        .get()
        .then(function(snapshot){
            var answer_promises = [];
            var answer_divs = [];
            $('#quest_num').html(snapshot.docs.length);
            if(snapshot.docs.length){
                var active = true;
                var index = 1;
                $('.trivia_content .question_holder').remove();
                snapshot.docs.forEach(function(doc){
                    var data = doc.data();
                    var slide = $('<div class="question_holder" data-question_id="'+doc.id+'">').data('question',data.question);
                    if(active) {
                        slide.addClass('active_quest');
                        curr_question_id = doc.id;
                        active = false;
                    }
                    var question = $('<div>').addClass('question');
                    question.append(
                        $('<p>').html('<span>'+index+'</span>. '+data.question)
                    )
                    slide.append(question);

                    if(data.question_type == 1){
                        var answers_div =  $('<div>').addClass('answer click_action');
                        answer_divs.push(answers_div);
                        answer_promises.push(
                        db.collection('question')
                        .doc(doc.id)
                        .collection('answers')
                        .orderBy('value','asc')
                        .get());
                        slide.append(answers_div);
                    } else if(data.question_type == 2){
                        slide.append('<textarea placeholder="Optional Comments">');
                        slide.append($('<button>').addClass('button white').text('NEXT'));
                    }                
                    index ++;
                    slide.insertBefore('.trivia_content > .success');
                });
                $('#question_country').html('<span>1</span> / '+index);
                Promise.all(answer_promises).then(snapshots => {
                    for(key in snapshots){
                        var snapshot = snapshots[key];
                        var target_div = answer_divs[key];
                        snapshot.docs.forEach(function(doc , i){
                            var data = doc.data();
                            target_div.append(
                                $('<p>')
                                .append($("<i data-num=" + i + ">").addClass('far fa-star').data('val',data.value).data('ans_id',doc.id))
                                .append($('<span>').html(data.text))
                            )
                        });
                    }
                    $('.links .button').css('pointer-events', 'all');
                    $('.load').removeClass('start_load');
                });
            }
        });
    
    }
    function nextQuestion( el ){
    	var quest = $(el).parents('.question_holder');
    	var timer;
    	var quest_num = $(quest).next('.question_holder').children('.question').children('p').children('span').html();
    	checkLast( quest_num );
    	
    	timer = setTimeout(function(){
	    	$(quest).fadeOut(500);
    	}, 200);

    	timer = setTimeout(function(){
	    	$('.title_holder p span#curr_quest').html(quest_num);
            $(quest).next('.question_holder').fadeIn(500);
            curr_question_id = $(quest).next('.question_holder').data('question_id');
	    	$('.answer p').css('pointer-events', 'all');
	    	$(quest).remove();
    	}, 700);
    }
    function checkLast( num ) {
    	if ( $('.question_holder').length == 1 ) {
    		setTimeout(function(){
    			$('.success').fadeIn();
    			setTimeout(function(){
	    			window.location = '/'
	    		}, 2000);
    		}, 700);
    	}
    }
    function set_listeners(){
        $(document).on('click', '.answer p', function() {
            $('.answer p').css('pointer-events', 'none');
            star = $(this).children('.fa-star');
            num = parseInt($(star).attr('data-num'))+1;
            var timer;

            $('.fa-star:lt(' + num + ')').removeClass('far').addClass('fas');
            nextQuestion( this );
            let question_text = $(this).parents('.question_holder').data('question');
            let val = star.data('val');
            let ans_id = star.data('ans_id');
            db.collection('response').doc().set({
                question_text: question_text,
                answer_text: val,
                answer_ref: '/question/'+curr_question_id+'/answers/'+ans_id,
                question_ref: '/question/'+curr_question_id,
                created: firebase.database.ServerValue.TIMESTAMP 
            })
            .then(function() {
                console.log("Document successfully written!");
            })
            .catch(function(error) {
                console.error("Error writing document: ", error);
            });
        });

        $(document).on('click', '.question_holder .button', function() {
            $(this).css('pointer-events', 'none');
            nextQuestion( this );
            let val = $(this).prev('textarea').val();
            let question_text = $(this).parents('.question_holder').data('question');
            if(val != '') {
                db.collection('response').doc().set({
                    question_text: question_text,
                    answer_text: val,
                    question_ref: '/question/'+curr_question_id
                })
                .then(function() {
                    console.log("Document successfully written!");
                })
                .catch(function(error) {
                    console.error("Error writing document: ", error);
                });
            }
        });
    }
    </script>  
</div>



 