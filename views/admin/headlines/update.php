<div class="card card-nav-tabs">
    <div class="card-header card-header-primary">
        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">general</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#images" data-toggle="tab">images <span id="images_count" class="em-notification">0</span></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="card-body ">
        <div class="tab-content text-center">
            <div class="tab-pane active" id="general">
				<form id='<?=$model->collection?>_form'>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Employee ID number</label>
								<input type="text" name="employee_id" class="form-control" placeholder="Employee ID number"/>
								<input type="hidden" name="image" id="main_image" /> 
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-check">
								<label class="form-check-label">
									<input type="hidden" name="featured" value="null"/>
									<input type="checkbox" name="featured" class="form-check-input" />
									Employee of the Month?
									<span class="form-check-sign">
										<span class="check"></span>
									</span>
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">First Name</label>
								<input type="text" name="first_name" class="form-control" placeholder="First Name the employee"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Last Name</label>
								<input type="text" name="last_name" class="form-control" placeholder="Last Name of the employee">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Email</label>
								<input type="email" name="email" class="form-control" placeholder="Email Address of the employee"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Phone</label>
								<input type="tel" name="phone" class="form-control" placeholder="Phone Number of the employee">
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-success btn_save" value="Save" />
				</form>
			</div>
			<div class="tab-pane" id="images">
				<form id="new_image_form" enctype="multipart/form-data">
					<div class="form-group form-file-upload form-file-multiple bmd-form-group">
						<input type="file" id="new_image" name="photo" accept="image/*" class="inputFileHidden">
						<div class="input-group">
							<input type="text" readonly class="form-control inputFileVisible" placeholder="Upload image File">
							<span class="input-group-btn" id="save_image_btn">
								<button type="button" class="btn btn-fab btn-round btn-primary">
									<i class="material-icons">save</i>
								</button>
							</span>
						</div>
					</div>
				</form>
				<div class="row" id="imgs_row">
					<div class="col-md-4 img_card hidden card" >
						<div class="card-body">
							<img src="" />
						</div>
						<div class="card-footer">
							<div class="form-check">
								<label class="form-check-label">
									<input type="checkbox" name="url" class="form-check-input main_img" />
									<span class="form-check-sign">
										<span class="check"></span>
									</span>
								</label>
							</div>
							<button type="button" class="btn btn-danger btn_delete">Delete</button>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
  </div>
<?php footer();?>
<style>
	.hidden {
		display: none;
	}
</style>
<script type="text/javascript">
	var batch;
	var images_cnt = 0;
	var imageRef;
	var collection = '<?=$model->collection?>';
	var main_data = {};
	var main_doc = {};
	function init(){ 
		batch = db.batch();
		main_doc = db.collection(collection).doc(<?=isset($model->record_id)?"'$model->record_id'":''?>);
		main_doc.get()
		.then(function(doc) {
			if(!doc.exists){
				main_doc = db.collection(collection).doc();
			}
		});
		$('#'+collection+'_form').submit(function(e){
			e.preventDefault();
			var data = objectifyForm($('#'+collection+'_form').serializeArray());
			batch
			.set(main_doc, data, {merge: true})
			.commit()
			.then(function(){
				console.log("Document successfully written!");
				batch = db.batch(); 
			}).catch(function(error){
				alert('Failed to save Data!');
			});
		});
		$(document).on('change','.img_card .main_img[name=url]',function(){
			$('.img_card').addClass('disabled').find('.main_img:checkbox').not(this).attr('checked',false);
			$('#main_image').val('');
			if($(this).is(':checked')){
				$('.img_card').addClass('disabled').find('.main_img:checkbox').not(this).attr('checked',false);
				$(this).parents('.img_card').removeClass('disabled');
				$('#main_image').val(this.value);
			}
		})
		$('#save_image_btn').off().click(function(){
			if($('#new_image')[0].files.length){
				var formData = new FormData();
				formData.append('file', $('#new_image')[0].files[0]);

				$.ajax({
					url : '/admin/employees/upload_image',
					type : 'POST',
					data : formData,
					processData: false,  // tell jQuery not to process the data
					contentType: false,  // tell jQuery not to set contentType
					success : function(data) {
						console.log(data);
						if(data.status){
							var image_url = '/content/uploads/employee/'+data.image.fileName;
							save_new_image(image_url);
						}
					}
				});
			} else {
				$('#new_image').click()
			}
		});
		load_document();
	}
	function load_document(){
		main_doc 
		.get()
		.then(function(doc) {
			if(doc.exists) {
				main_data = doc.data();
			} else {
				main_data = {}
			}
			var form = $('#'+collection+'_form');
			for(var key in main_data ){
				if(typeof main_data[key] !== 'object' ){
					var input = form.find('[name='+key+']').not('[type=hidden]');
					if(input.is('[type=checkbox],[type=radio]')){
						input.val() == main_data[key];
						input.attr('checked','checked');
					} else {
						input.val(main_data[key]);
					}
				}
			}
			build_images();
		});

	}
	function save_new_image(url){ //Accepts jQuery element of card
		var img_doc = main_doc.collection('images').doc();
		var img_data = {url: url};
		main_doc.get().then(function(doc){
			if(doc.exists){
				img_doc.set(img_data,{merge:true}).then(function(){
					console.log('success');
					build_images();
				});
			} else {
				batch.set(img_doc, img_data, {merge: true});
				alert('You must finish saving this new client to save your document changes');
			}
		});
	}
	function build_images(){
		// imageRef = storage.ref().child('images/'+collection+'/'+main_doc.id+'/');
		images_cnt = 0;
		// $('#new_doc_btn').prop('disabled',false);
		$('#imgs_row .img_card').not('.hidden').remove();
		main_doc.collection('images')
		.get().then((docs) => {
			docs.forEach(function(_doc){
				images_cnt++;
				$('#images_count').text(images_cnt);
				img = _doc.data();
				var img_card = $('.img_card.hidden').clone();
				img_card.removeClass('hidden');
				img_card.data('id',_doc.id);
				img_card.find('img') 
				.attr('src',img.url);
				img_card
				.find(':input[name=url]')
				.val(img.url);
				$('#imgs_row').append(img_card);
			});
		});
	}
	// function build_contacts(){
	// 	contacts_cnt = 0;
	// 	$('#new_contact_btn').prop('disabled',false);
	// 	$('#contacts_row .contact_card').not('.hidden').remove();
	// 	main_doc.collection('contacts')
	// 	.get().then((contacts) => {
	// 		contacts.forEach(function(_contact){
	// 			contacts_cnt ++;
	// 			$('#contacts_count').text(contacts_cnt);
	// 			contact = _contact.data();
	// 			var contact_card = $('.contact_card.hidden').clone();
	// 			contact_card.removeClass('hidden');
	// 			contact_card.data('id',_contact.id);
	// 			for(i in contact){
	// 				contact_card.find('[name='+i+']').val(contact[i]);
	// 			}
	// 			$('#contacts_row').append(contact_card);
	// 		});
	// 	});
	// }
	// function new_contact(){
	// 	// $('#new_contact_btn').prop('disabled',true);
	// 	var empty_contact = $('.contact_card.hidden').clone();
	// 	empty_contact.removeClass('hidden');
	// 	$('#contacts_row').append(empty_contact);
	// }
	// function new_document(){
	// 	// $('#new_contact_btn').prop('disabled',true);
	// 	var empty_doc = $('.doc_card.hidden').clone();
	// 	empty_doc.removeClass('hidden');
	// 	$('#docs_row').append(empty_doc);
	// }
	// function save_document(doc_card){ //Accepts jQuery element of card
	// 	var doc_data = objectifyForm(doc_card.find(':input').serializeArray());
	// 	var doc_id = doc_card.data('id');
	// 	var doc_doc;
	// 	if(doc_id){
	// 		doc_doc = main_doc.collection('documents').doc(doc_id);
	// 	} else {
	// 		doc_doc = main_doc.collection('documents').doc();
	// 	}
	// 	main_doc.get().then(function(doc){
	// 		if(doc.exists){
	// 			doc_doc.set(doc_data,{merge:true}).then(function(){
	// 				console.log('success');
	// 				build_docs();
	// 			});
	// 		} else {
	// 			batch.set(doc_doc, doc_data, {merge: true});
	// 			alert('You must finish saving this new client to save your document changes');
	// 		}
	// 	});
	// }
	// function save_contact(contact_card){ //Accepts jQuery element of card
	// 	var contact_data = objectifyForm(contact_card.find(':input').serializeArray());
	// 	var contact_id = contact_card.data('id');
	// 	var contact_doc;
	// 	if(contact_id){
	// 		contact_doc = main_doc.collection('contacts').doc(contact_id);
	// 	} else {
	// 		contact_doc = main_doc.collection('contacts').doc();
	// 	}
	// 	main_doc.get().then(function(doc){
	// 		if(doc.exists){
	// 			contact_doc.set(contact_data,{merge:true}).then(function(){
	// 				console.log('success');
	// 				build_contacts();
	// 			});
	// 		} else {
	// 			batch.set(contact_doc, contact_data, {merge: true});
	// 			alert('You must finish saving this new client to save your contact changes');
	// 		}
	// 	});
	// }
</script>