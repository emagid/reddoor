<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> View responses to survey questions</h4>
          </div>
          <div class="card-body">
            <select id='questions'  class="form-control">
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id='data-cards'>

      <div class="col-md-6 chart_card">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Regular header</h4>
          </div>
          <div class="card-body">
            <canvas class="bar_chart"  width="400" height="400">
            </canvas>
          </div>
        </div>
      </div>

      <div class="col-md-6 responses_card">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Regular header</h4>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <tr>
                  <th>Answer</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody class="responses">
                <tr>
                  <td>1</td>
                  <td>Dakota Rice</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <div class="stats pagination">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
<script type="text/javascript">
    var collection = '<?=$model->collection?>';
    var barChartCard;
    var historyCard;
    function build_collection(list_elem,ref){
      ref.orderBy('value','asc')
      .get()
      .then(function(snapshot){
        
        snapshot.forEach(function(doc) {
          var item = doc.data();
          list_elem.append($('<li>').text(item.text));
        })
        
      });

    }
    function build_row(doc){ // Accepts firestore doc references
      var row =  $('<tr class="<?=$model->collection?>" draggable="true" ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)" >');
      row.data('id', doc.id);
      row.attr('id',doc.id);
      data = doc.data();
      structure.forEach(function(v){
        switch (v.data_type) {
          case 'number':
          case 'string':
            row.append('<td>'+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            break;
          case 'option':
            row.append('<td>'+(data[v.field_name]?v.data_options[data[v.field_name]]:'')+'</td>');
            break;   
          case 'image':
            // row.append('<td><img src='+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            row.append('<td><img src="'+(data[v.field_name]?data[v.field_name]:'')+'" width="50%" /></td>');
            break;
          case 'boolean':
            row.append('<td><i class="material-icons">'+(data[v.field_name]?'done':'')+'</i></td>');
            break;
          case 'collection':
            var list = $('<ol></ol>');
            row.append($('<td>').append(list));
            build_collection(list,db.collection(collection).doc(doc.id).collection(v.field_name));
            break;
        }
      });
      // row.append('<td>'+(data.employee_id?data.employee_id:'')+'</td>');
      // row.append('<td>'+(data.first_name?data.first_name:'')+'</td>');
      // row.append('<td>'+(data.last_name?data.last_name:'')+'</td>');
      // row.append('<td>'+(data.email?data.email:'')+'</td>');
      // row.append('<td><i class="material-icons">'+(data.featured?'done':'')+'</i></td>');
      
      if(curr_user.user_role == 'superadmin'){
        row.append('<td><a class="btn" href="/admin/<?=$this->emagid->route['controller']?>/update/'+doc.id+'"> Edit</a></td>');
        row.append('<td><a class="btn delete"> delete</a></td>');
      } else {
        row.append('<td></td>');
        row.append('<td></td>');
      }
      return row;
    }
    function loadQuestions(){
      db.collection('question')
      .onSnapshot(function(snapshot){
        $('#questions').empty();
        var val = $('select#questions').val();
        snapshot.docs.forEach(function(doc){
          var data = doc.data();
          $('select#questions').append('<option value="'+doc.id+'" data-type="'+data.question_type+'">'+data.question+'</option>');
          // questions.push({id: doc.id, text: data.question, 'data-type': data.question_type});
        });
        $('select#questions').change();
      });
    } 
    function loadAnswers(question_id,type){
      console.log(question_id);
      console.log(type);

      db.collection('response')
      .where("question_ref","==","/question/"+question_id)
      .onSnapshot(function(snapshot){

        var labels = [];
        var  counts = {};
        var  values = [];
        var colors = []; 
        var colors2 = [];
        var newBarCard = barChartCard.clone(); 
        var newRespCard = historyCard.clone(); 

        $('#data-cards').empty();
        snapshot.docs.forEach(function(doc){
          var data = doc.data();
          if(!counts.hasOwnProperty(data.answer_text)){
            counts[data.answer_text] = 1;
            colors.push('rgba(255, 99, 132, 0.2)');
            colors2.push('rgba(255,99,132,1)');
          } else {
            counts[data.answer_text]++;
          }
        });
        for(key in counts){
          labels.push(key);
          values.push(counts[key]);
        }
        ctx = newBarCard.find('canvas');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: labels,
              datasets: [{
                  label: '# of Answers',
                  data: values,
                  backgroundColor: colors,
                  borderColor: colors2,
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
        });
        newBarCard.appendTo('#data-cards');
      });
    }
    function init(){
      
      barChartCard = $('#data-cards .chart_card').clone();
      historyCard = $('#data-cards .responses_card').clone();
      $('#data-cards').empty();
      $('select#questions').select2({
        placeholder: "Select a question to view the results",
      });
      loadQuestions();
      
      if(curr_user.user_role == 'superadmin'){
        
        $('select#questions').change(function(){
          var question = this.value;
          var type = $(this).find('option:selected').data('type');
          loadAnswers(question,type)
        });

        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var row = $(this).parents('tr.'+collection);
          db.collection(collection).doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var data = {};
          var row = $(this).parents('tr.'+collection);
          // row.find(':input').serializeArray().forEach(function(elem) {
          //   if(elem.name != 'id'){
          //     data[elem.name] = (elem.value == ''?null:elem.value
          //   }
          // });
          db.collection(collection).doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        jQuery.fn.swap = function(target){
            var one = this;
            var two = target;
            var onePrev = one.prev();
          if( onePrev.length < 1 ) var oneParent = one.parent();
            var twoPrev = two.prev();
          if( twoPrev.length < 1 ) var twoParent = two.parent();
          
          if( onePrev.length > 0 ) onePrev.after( two );
              else oneParent.prepend( two );
            
          if( twoPrev.length > 0 ) twoPrev.after( one );
              else twoParent.prepend( one );
        
          return this;
        }
        // $(document).on('change','textarea.notes',function(){
        //   var id = $(this).parents('tr.user').data('id');
        //   db.collection('users').doc(id).set({
				// 		notes: this.value
				// 	},{ merge: true }).then(function(){
				// 	});
        // });
      }
    }

    function allowDrop(ev){
      ev.preventDefault();
    }
    function drag(ev) {
        ev.dataTransfer.setData("row", ev.target.id);
    }

    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("row");
        var moving = $('.<?=$model->collection?>#'+data);
        if(!ev.target.id){
          var target = $(ev.target).parents('tr.<?=$model->collection?>');
        } else {
          var target = $('.<?=$model->collection?>#'+ev.target.id);
        }
        moving.insertBefore(target);
        var batch = db.batch();
        $('tr.<?=$model->collection?>[id]').each(function(i){
          batch.set(db.collection(collection).doc(this.id), {display_order: i+1}, {merge: true});
        });
        batch
        .commit()
        .then(function(){
          console.log("Document successfully written!");
          loadCollection();
        }).catch(function(error){
          loadCollection();
        });
    }
</script>