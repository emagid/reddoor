<div class="card card-nav-tabs">
    <div class="card-header card-header-primary">
        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">general</a>
                    </li>
                    <li id="answer_tab_link" class="nav-item hidden">
                        <a class="nav-link" href="#answers" data-toggle="tab">Answers <span id="answer_count" class="em-notification">0</span></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="card-body ">
        <div class="tab-content text-center">
            <div class="tab-pane active" id="general">
				<form id='<?=$model->collection?>_form'>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Question Text</label>
								<input type="text" name="question" class="form-control" placeholder="Question Text"/>
								<input type="hidden" name="display_order" value="0" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Question type</label>
								<select name="question_type" class="form-control">
									<option value="" selected disabled>-- Select a question type --</option>
									<option value="1">Rating</option>
									<option value="2">Written</option>
								</select>
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-success btn_save" value="Save" />
				</form>
			</div>
			<div class="tab-pane" id="answers">
				<button type="button" class="btn btn-success btn-round" id="new_ans_btn"><i class="material-icons">+</i></button>
				<div class="row" id="ans_row">
					<div class="col-md-6 ans_card hidden card" >
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Answer Text</label>
										<input type="text" name="text" class="form-control" placeholder="Answer Text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Answer Value</label>
										<input type="number" name="value" class="form-control" placeholder="Answer Value">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="btn btn-primary btn_save">Save</button>
							<button type="button" class="btn btn-danger  btn_delete">Delete</button>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
  </div>
<?php footer();?>
<style>
	.hidden {
		display: none;
	}
</style>
<script type="text/javascript">
	var batch;
	var answers_cnt = 0;
	var imageRef;
	var collection = '<?=$model->collection?>';
	var main_data = {};
	var main_doc = {};
	function init(){ 
		batch = db.batch();
		main_doc = db.collection(collection).doc(<?=isset($model->record_id)?"'$model->record_id'":''?>);
		main_doc.get()
		.then(function(doc) {
			if(!doc.exists){
				main_doc = db.collection(collection).doc();
			}
		});
		$('[name=question_type]').change(function(){
			if($(this).val() == 1){
				$('#answer_tab_link').removeClass('hidden');
			} else {
				$('#answer_tab_link').addClass('hidden');
			}
		});
		$('#'+collection+'_form').submit(function(e){
			e.preventDefault();
			var data = objectifyForm($('#'+collection+'_form').serializeArray());
			batch
			.set(main_doc, data, {merge: true})
			.commit()
			.then(function(){
				console.log("Document successfully written!");
				batch = db.batch(); 
			}).catch(function(error){
				alert('Failed to save Data!');
			});
		});
		$(document).on('click','.ans_card .btn_save',function(){
			var card = $(this).parents('.ans_card');
			save_answer(card);
		});
		$(document).on('click','.ans_card .btn_delete', function(){
			var card = $(this).parents('.ans_card');
			delete_answer(card);
		});
		$('#new_ans_btn').not('[disabled]').click(function(){
			new_answer();
		});
		load_document();
	}
	function load_document(){
		main_doc 
		.get()
		.then(function(doc) {
			if(doc.exists) {
				main_data = doc.data();
			} else {
				main_data = {}
			}
			var form = $('#'+collection+'_form');
			for(var key in main_data ){
				if(typeof main_data[key] !== 'object' ){
					var input = form.find('[name='+key+']').not('[type=hidden]');
					if(input.is('[type=checkbox],[type=radio]')){
						input.val() == main_data[key];
						input.attr('checked','checked');
					} else {
						input.val(main_data[key]).change();
					}
				}
			}
			build_answers();
		});

	}
	function delete_answer(ans_card){
		var ans_id = ans_card.data('id');
		main_doc.collection('answers').doc(ans_id).delete().then(function() {
			ans_card.remove();
			console.log("Document successfully deleted!");
			build_answers();
		}).catch(function(error) {
			alert.error("Error removing document: ", error);
		}); 
	}
	function new_answer(){
		var empty_ans = $('.ans_card.hidden').clone();
		empty_ans.removeClass('hidden').find('[name=value]').val(answers_cnt+1);
		$('#ans_row').append(empty_ans);
	}
	function save_answer(ans_card){ //Accepts jQuery element of card
		var ans_data = objectifyForm(ans_card.find(':input').serializeArray());
		var ans_id = ans_card.data('id');
		var ans_doc;
		if(ans_id){
			ans_doc = main_doc.collection('answers').doc(ans_id);
		} else {
			ans_doc = main_doc.collection('answers').doc();
		}
		main_doc.get().then(function(doc){
			if(doc.exists){
				ans_doc.set(ans_data,{merge:true}).then(function(){
					console.log('success');
					build_answers();
				});
			} else {
				batch.set(ans_doc, ans_data, {merge: true});
				alert('You must finish saving this new client to save your document changes');
			}
		});
	}
	function build_answers(){
		answers_cnt = 0;
		// $('#new_doc_btn').prop('disabled',false);
		$('#ans_row .ans_card').not('.hidden').remove();
		main_doc.collection('answers')
		.orderBy('value','asc')
		.get().then((docs) => {
			docs.forEach(function(_doc){
				answers_cnt++;
				$('#answer_count').text(answers_cnt);
				var answer = _doc.data();
				var ans_card = $('.ans_card.hidden').clone();
				ans_card.removeClass('hidden');
				ans_card.data('id',_doc.id);
				for(i in answer){
					ans_card.find('[name='+i+']').val(answer[i]);
				}
				$('#ans_row').append(ans_card);
			});
		});
	}
	// function build_contacts(){
	// 	contacts_cnt = 0;
	// 	$('#new_contact_btn').prop('disabled',false);
	// 	$('#contacts_row .contact_card').not('.hidden').remove();
	// 	main_doc.collection('contacts')
	// 	.get().then((contacts) => {
	// 		contacts.forEach(function(_contact){
	// 			contacts_cnt ++;
	// 			$('#contacts_count').text(contacts_cnt);
	// 			contact = _contact.data();
	// 			var contact_card = $('.contact_card.hidden').clone();
	// 			contact_card.removeClass('hidden');
	// 			contact_card.data('id',_contact.id);
	// 			for(i in contact){
	// 				contact_card.find('[name='+i+']').val(contact[i]);
	// 			}
	// 			$('#contacts_row').append(contact_card);
	// 		});
	// 	});
	// }
	// function new_contact(){
	// 	// $('#new_contact_btn').prop('disabled',true);
	// 	var empty_contact = $('.contact_card.hidden').clone();
	// 	empty_contact.removeClass('hidden');
	// 	$('#contacts_row').append(empty_contact);
	// }
	// function new_document(){
	// 	// $('#new_contact_btn').prop('disabled',true);
	// 	var empty_doc = $('.doc_card.hidden').clone();
	// 	empty_doc.removeClass('hidden');
	// 	$('#docs_row').append(empty_doc);
	// }
	// function save_document(doc_card){ //Accepts jQuery element of card
	// 	var doc_data = objectifyForm(doc_card.find(':input').serializeArray());
	// 	var doc_id = doc_card.data('id');
	// 	var doc_doc;
	// 	if(doc_id){
	// 		doc_doc = main_doc.collection('documents').doc(doc_id);
	// 	} else {
	// 		doc_doc = main_doc.collection('documents').doc();
	// 	}
	// 	main_doc.get().then(function(doc){
	// 		if(doc.exists){
	// 			doc_doc.set(doc_data,{merge:true}).then(function(){
	// 				console.log('success');
	// 				build_docs();
	// 			});
	// 		} else {
	// 			batch.set(doc_doc, doc_data, {merge: true});
	// 			alert('You must finish saving this new client to save your document changes');
	// 		}
	// 	});
	// }
	// function save_contact(contact_card){ //Accepts jQuery element of card
	// 	var contact_data = objectifyForm(contact_card.find(':input').serializeArray());
	// 	var contact_id = contact_card.data('id');
	// 	var contact_doc;
	// 	if(contact_id){
	// 		contact_doc = main_doc.collection('contacts').doc(contact_id);
	// 	} else {
	// 		contact_doc = main_doc.collection('contacts').doc();
	// 	}
	// 	main_doc.get().then(function(doc){
	// 		if(doc.exists){
	// 			contact_doc.set(contact_data,{merge:true}).then(function(){
	// 				console.log('success');
	// 				build_contacts();
	// 			});
	// 		} else {
	// 			batch.set(contact_doc, contact_data, {merge: true});
	// 			alert('You must finish saving this new client to save your contact changes');
	// 		}
	// 	});
	// }
</script>