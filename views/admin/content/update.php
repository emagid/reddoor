<div class="card card-nav-tabs">
    <div class="card-header card-header-primary">
        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">general</a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="card-body ">
        <div class="tab-content text-center">
            <div class="tab-pane active" id="general">
				<form id='<?=$model->collection?>_form' enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Title</label>
								<input type="text" name="title" class="form-control" placeholder="Smaller, upper title of media"/>
								<input type="hidden" name="display_order" value="0" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="bmd-label-static">Content Page</label>
								<select name="page" class="form-control">
									<option value="" selected disabled>-- Content Page --</option>
									<option value="1">Core Values</option>
									<option value="2">Human Resources</option>
									<option value="3">Working At The Red Door</option>
									<option value="4">What's Happening</option>
									<option value="5">Promotions</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-check">
								<label class="form-check-label">
									<input type="hidden" name="visible" value=""/>
									<input type="checkbox" name="visible" value="true" class="form-check-input" />
									Active?
									<span class="form-check-sign">
										<span class="check"></span>
									</span>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-file-upload form-file-multiple bmd-form-group">
								<input type="file" id="new_image" name="media" accept="image/*" class="inputFileHidden" />
								<div class="input-group">
									<input type="text" readonly class="form-control inputFileVisible" placeholder="Upload Thumbnail">
									<span class="input-group-btn" id="save_thumbnail_btn">
										<button type="button" class="btn btn-fab btn-round btn-primary">
											<i class="material-icons">save</i>
										</button>
									</span>
								</div>
							</div>
							<input type="hidden" name="thumbnail" />
							<div id="preview">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-file-upload form-file-multiple bmd-form-group">
								<input type="file" id="new_file" name="media" accept="image/*,.mp4,.pdf" class="inputFileHidden" />
								<div class="input-group">
									<input type="text" readonly class="form-control inputFileVisible" placeholder="Upload Media">
									<span class="input-group-btn" id="save_image_btn">
										<button type="button" class="btn btn-fab btn-round btn-primary">
											<i class="material-icons">save</i>
										</button>
									</span>
								</div>
							</div>
							<input type="hidden" name="url" />
							<div id="curr_media">
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-success btn_save" value="Save" />
				</form>
			</div>
        </div>
    </div>
  </div>
<?php footer();?>
<style>
	.hidden {
		display: none;
	}
</style>
<script type="text/javascript">
	var batch;
	var imageRef;
	var collection = '<?=$model->collection?>';
	var main_data = {};
	var main_doc = {};
	function init(){ 
		batch = db.batch();
		main_doc = db.collection(collection).doc(<?=isset($model->record_id)?"'$model->record_id'":''?>);
		main_doc.get()
		.then(function(doc) {
			if(!doc.exists){
				main_doc = db.collection(collection).doc();
			}
		});
		$('#'+collection+'_form').submit(function(e){
			e.preventDefault();
			var data = objectifyForm($('#'+collection+'_form').serializeArray());
			batch
			.set(main_doc, data, {merge: true});
				
			if($('#new_image')[0].files.length || $('#new_file')[0].files.length){
				var deferreds = [];
				if($('#new_file')[0].files.length) {
					var formData = new FormData();
					formData.append('file', $('#new_file')[0].files[0]);
					
					deferreds.push(
						$.ajax({
							url : '/admin/content/upload_image',
							type : 'POST',
							data : formData,
							processData: false,  // tell jQuery not to process the data
							contentType: false,  // tell jQuery not to set contentType
							success : function(data) {
								console.log(data);
								if(data.status){
									var image_url = '/content/uploads/content/'+data.image.fileName;
									batch.set(main_doc,{url: image_url},{merge: true});
									if(data.thumbnail){
										batch.set(main_doc,{thumbnail: data.thumbnail},{merge:true});
									}
								} else {
									if(confirm('Failed to Upload new pdf, Save anyway?')){
										batch.set(main_doc,{url: main_data.url},{merge:true});
									}
								}
							}
						})
					);
				}
				if($('#new_image')[0].files.length){ 
					var formData = new FormData();
					formData.append('file', $('#new_image')[0].files[0]);
					
					deferreds.push(
						$.ajax({
							url : '/admin/content/upload_image',
							type : 'POST',
							data : formData,
							processData: false,  // tell jQuery not to process the data
							contentType: false,  // tell jQuery not to set contentType
							success : function(data) {
								console.log(data);
								if(data.status){
									var image_url = '/content/uploads/content/'+data.image.fileName;
									batch.set(main_doc,{thumbnail: image_url},{merge: true});
								} else {
									if(confirm('Failed to Upload new thumbnail? Save anyway?')){
										batch.set(main_doc,{thumbnail: main_data.url},{merge:true});
									}
								}
							}
						})
					);
				}
				$.when(...deferreds).then(function(){
					batch.commit()
					.then(function(){
						console.log("Document successfully written!");
						batch = db.batch(); 
						load_document();
					}).catch(function(error){
						alert('Failed to save Data!');
						load_document();
					});
				});
			} else {
				batch.commit()
				.then(function(){
					console.log("Document successfully written!");
					batch = db.batch(); 
					load_document();
				}).catch(function(error){
					alert('Failed to save Data!');
					load_document();
				});
			}
			
		});
		$(document).on('change','#new_image,#new_file',function(){
			$(this).siblings('.input-group').find('.inputFileVisible').val($(this)[0].files[0].name);
		})
		load_document();
	}
	function load_document(){
		$('#new_image').replaceWith($('<input type="file" id="new_image" name="media" accept="image/*" class="inputFileHidden" />'));
		$('#new_file').replaceWith($('<input type="file" id="new_file" name="media" accept="image/*,.mp4,.pdf" class="inputFileHidden" />'));

		main_doc 
		.get()
		.then(function(doc) {
			if(doc.exists) {
				main_data = doc.data();
			} else {
				main_data = {}
			}
			var form = $('#'+collection+'_form');
			for(var key in main_data ){
				if(typeof main_data[key] !== 'object' ){
					var input = form.find('[name='+key+']').not('[type=hidden][value=""]');
					if(input.is('[type=checkbox],[type=radio]')){
						input.val() == main_data[key];
						if(main_data[key]){
							input.attr('checked','checked');
						}
					} else {
						input.val(main_data[key]).change();
						if(key == 'thumbnail' && main_data[key]){
							build_preview(main_data.thumbnail,1);
						}
					}
				}
			}
		});
	}
	function build_preview(url,data_type){
		$('#preview').empty();
		var preview;
		if(data_type == 1){
			preview = $('<img src="'+url+'" width="100%" />')
		} else {
			preview = $('<video class="video" width="100% type="video/mp4" muted="" src="'+url+'" width="60px" ></video>')
		}
		$('#preview').append(preview);
		$('#curr_media').empty();
		$('#curr_media').append($('<a>').attr('href',main_data.url).text(main_data.url));
	}
</script>