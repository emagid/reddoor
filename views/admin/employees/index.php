<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> View Users</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th> Employee ID</th>
                  <th> First Name</th>
                  <th> Last Name </th>
                  <th> Phone</th>
                  <th> Email</th>
                  <th> Image</th>
                  <th> Featured? </th>
                  <th colspan='2'> edit </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    var collection = '<?=$model->collection?>';
    var structure = [
      {
        field_name: 'employee_id',
        data_type: 'string' 
      },
      {
        field_name: 'first_name',
        data_type: 'string' 
      },
      {
        field_name: 'last_name',
        data_type: 'string' 
      },
      {
        field_name: 'phone',
        data_type: 'string' 
      },
      {
        field_name: 'email',
        data_type: 'string' 
      },
      {
        field_name: 'image',
        data_type: 'image' 
      },
      {
        field_name: 'featured',
        data_type: 'boolean' 
      }
    ];
    function build_row(doc){ // Accepts firestore doc references
      var row =  $('<tr class="<?=$model->collection?>">');
      row.data('id', doc.id);
      data = doc.data();
      structure.forEach(function(v){
        switch (v.data_type) {
          case 'string':
            row.append('<td>'+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            break; 
          case 'image':
            // row.append('<td><img src='+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            row.append('<td><img src="'+(data[v.field_name]?data[v.field_name]:'')+'" width="50%" /></td>');
            break;
          case 'boolean':
            row.append('<td><i class="material-icons">'+(data[v.field_name]?'done':'')+'</i></td>');
            break;
        }
      });
      // row.append('<td>'+(data.employee_id?data.employee_id:'')+'</td>');
      // row.append('<td>'+(data.first_name?data.first_name:'')+'</td>');
      // row.append('<td>'+(data.last_name?data.last_name:'')+'</td>');
      // row.append('<td>'+(data.email?data.email:'')+'</td>');
      // row.append('<td><i class="material-icons">'+(data.featured?'done':'')+'</i></td>');
      
      if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
        row.append('<td><a class="btn" href="/admin/<?=$this->emagid->route['controller']?>/update/'+doc.id+'"> Edit</a></td>');
        row.append('<td><a class="btn delete"> delete</a></td>');
      } else {
        row.append('<td></td>');
        row.append('<td></td>');
      }
      return row;
    }
    function loadCollection(){
      db.collection(collection)
        .get().then(function(querySnapshot) {
        $('.items').empty();
        querySnapshot.forEach(function(doc) {
          var row = build_row(doc);
          $('.items').append(row);

        });

        var new_row = $('<tr class="'+collection+'">');
        var name = collection[0].toUpperCase() + collection.substring(1);

        new_row.append('<td colspan="' + structure.length + '"> New '+name+' </td>');
        new_row.append('<td colspan="2"><a class="btn" href="/admin/<?=$this->emagid->route['controller']?>/update/"> Add</a></td>');

        if(curr_user.user_role == 'superadmin'){
          $('.items').append(new_row);
        }

      });
    } 
    function init(){
      
      loadCollection();
      
      if(curr_user.user_role == 'superadmin'){
        // $(document).on('click','a.btn.add',function(){
        //   var data = {};
        //   var row = $(this).parents('tr.'+collection);
        //   var id;
        //   row.find(':input').serializeArray().forEach(function(elem) {
        //     if(elem.name != 'id'){
        //       data[elem.name] = (elem.value == ''?null:elem.value);
        //     } else {
        //       id = elem.value;
        //     }
        //   });
        //   db.collection(collection)
        //    .add(data)
        //    .then(function(){
        //      loadCollection();
        //    }); 
        // });

        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var row = $(this).parents('tr.'+collection);
          db.collection(collection).doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var data = {};
          var row = $(this).parents('tr.'+collection);
          // row.find(':input').serializeArray().forEach(function(elem) {
          //   if(elem.name != 'id'){
          //     data[elem.name] = (elem.value == ''?null:elem.value
          //   }
          // });
          db.collection(collection).doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        // $(document).on('change','textarea.notes',function(){
        //   var id = $(this).parents('tr.user').data('id');
        //   db.collection('users').doc(id).set({
				// 		notes: this.value
				// 	},{ merge: true }).then(function(){
				// 	});
        // });
      }
    }
</script>