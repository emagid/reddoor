<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> View Users</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th> Title</th>
                  <th> SubTitle</th>
                  <th> Display Order </th>
                  <th> Active </th>
                  <th> Display Time </th>
                  <th> Type </th>
                  <th> Preview </th>
                  <th colspan='2'> edit </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    var collection = '<?=$model->collection?>';
    var structure = [
      {
        field_name: 'title',
        data_type: 'string' 
      },
      {
        field_name: 'subtitle',
        data_type: 'string' 
      },
      {
        field_name: 'display_order',
        data_type: 'number' 
      },
      {
        field_name: 'visible',
        data_type: 'boolean' 
      },
      {
        field_name: 'display_time',
        data_type: 'number' 
      },
      {
        field_name: 'banner_type',
        data_type: 'option',
        data_options: {
          "1": "Image",
          "2": "Video"
        } 
      },      
      {
        field_name: 'url',
        data_type: 'ref',
        data_ref: 'banner_type',
        data_options: {
          "1": "image",
          "2": "video"
        }  
      }
    ];
    function build_row(doc){ // Accepts firestore doc references
      var row =  $('<tr class="<?=$model->collection?>" draggable="true" ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)" >');
      row.data('id', doc.id);
      row.attr('id',doc.id);
      let data = doc.data();
      structure.forEach(function(v){
        let switchType = v.data_type;
        if(v.data_type == 'ref'){
          switchType = v.data_options[data[v.data_ref]];
        }
        switch (switchType) {
          case 'number':
          case 'string':
            row.append('<td>'+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            break; 
          case 'option':
            row.append('<td>'+(data[v.field_name]?v.data_options[data[v.field_name]]:'')+'</td>');
            break;  
          case 'image':
            // row.append('<td><img src='+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            row.append('<td><img src="'+(data[v.field_name]?data[v.field_name]:'')+'" width="50%" /></td>');
            break;  
          case 'video':
            // row.append('<td><img src='+(data[v.field_name]?data[v.field_name]:'')+'</td>');
            row.append('<td><video class="video" width="50%" type="video/mp4" src="'+(data[v.field_name]?data[v.field_name]:'')+'" muted="" loop=""></video></td>');
            break;
          case 'boolean':
            row.append('<td><i class="material-icons">'+(data[v.field_name]?'done':'')+'</i></td>');
            break;
        }
      });
      // row.append('<td>'+(data.employee_id?data.employee_id:'')+'</td>');
      // row.append('<td>'+(data.first_name?data.first_name:'')+'</td>');
      // row.append('<td>'+(data.last_name?data.last_name:'')+'</td>');
      // row.append('<td>'+(data.email?data.email:'')+'</td>');
      // row.append('<td><i class="material-icons">'+(data.featured?'done':'')+'</i></td>');
      
      if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
        row.append('<td><a class="btn" href="/admin/<?=$this->emagid->route['controller']?>/update/'+doc.id+'"> Edit</a></td>');
        row.append('<td><a class="btn delete"> delete</a></td>');
      } else {
        row.append('<td></td>');
        row.append('<td></td>');
      }
      return row;
    }
    function loadCollection(){
      db.collection(collection)
        .orderBy('display_order','asc')
        .get().then(function(querySnapshot) {
        $('.items').empty();
        querySnapshot.forEach(function(doc) {
          var row = build_row(doc);
          $('.items').append(row);

        });

        var new_row = $('<tr class="'+collection+'">');
        var name = collection[0].toUpperCase() + collection.substring(1);

        new_row.append('<td colspan="' + structure.length + '"> New '+name+' </td>');
        new_row.append('<td colspan="2"><a class="btn" href="/admin/<?=$this->emagid->route['controller']?>/update/"> Add</a></td>');

        if(curr_user.user_role == 'superadmin'){
          $('.items').append(new_row);
        }

      });
    } 
    function init(){
      
      loadCollection();
      
      if(curr_user.user_role == 'superadmin'){
        // $(document).on('click','a.btn.add',function(){
        //   var data = {};
        //   var row = $(this).parents('tr.'+collection);
        //   var id;
        //   row.find(':input').serializeArray().forEach(function(elem) {
        //     if(elem.name != 'id'){
        //       data[elem.name] = (elem.value == ''?null:elem.value);
        //     } else {
        //       id = elem.value;
        //     }
        //   });
        //   db.collection(collection)
        //    .add(data)
        //    .then(function(){
        //      loadCollection();
        //    }); 
        // });

        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var row = $(this).parents('tr.'+collection);
          db.collection(collection).doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.'+collection).data('id');
          var data = {};
          var row = $(this).parents('tr.'+collection);
          // row.find(':input').serializeArray().forEach(function(elem) {
          //   if(elem.name != 'id'){
          //     data[elem.name] = (elem.value == ''?null:elem.value
          //   }
          // });
          db.collection(collection).doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        // $(document).on('change','textarea.notes',function(){
        //   var id = $(this).parents('tr.user').data('id');
        //   db.collection('users').doc(id).set({
				// 		notes: this.value
				// 	},{ merge: true }).then(function(){
				// 	});
        // });
      }
    }
    function allowDrop(ev){
      ev.preventDefault();
    }
    function drag(ev) {
        ev.dataTransfer.setData("row", ev.target.id);
    }

    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("row");
        var moving = $('.<?=$model->collection?>#'+data);
        if(!ev.target.id){
          var target = $(ev.target).parents('tr.<?=$model->collection?>');
        } else {
          var target = $('.<?=$model->collection?>#'+ev.target.id);
        }
        moving.insertBefore(target);
        var batch = db.batch();
        $('tr.<?=$model->collection?>[id]').each(function(i){
          batch.set(db.collection(collection).doc(this.id), {display_order: i+1}, {merge: true});
        });
        batch
        .commit()
        .then(function(){
          console.log("Document successfully written!");
          loadCollection();
        }).catch(function(error){
          loadCollection();
        });
    }
</script>