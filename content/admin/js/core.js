var curr_user;
var db;
$(document).ready(function(){
    firebase.initializeApp(config);
    db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            db.collection('users').doc(user.uid).get().then(function(_user){
                curr_user = _user.data();
                curr_user.displayName = firebase.auth().currentUser.displayName; 
                curr_user.phoneNumber = firebase.auth().currentUser.phoneNumber; 
                init();
            });
        } else {
          if(window.location.pathname != "/admin/login"){     
            window.location.href = "/admin/login/logout";
          } else {
            init();
          }
        }
    });
 });
 function objectifyForm(formArray) {//serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
      returnArray[formArray[i]['name']] = formArray[i]['value']?formArray[i]['value']:null;
    }
    return returnArray;
  }