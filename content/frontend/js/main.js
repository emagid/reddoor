var db;

// Vue.mixin({
//   data: function(){
//     firebase.initializeApp(config);
//     db = firebase.firestore();
//     db.settings({
//         timestampsInSnapshots: true
//     });

//     return {
//         d: db
//     }
//   }
// })

$(document).ready(function(){
    
    function headerInit(){
        db.collection('employee')
        .where('featured','==','on')
        .limit(1)
        .onSnapshot(function(snapshot){
            if(snapshot.docs.length){            
                var doc = snapshot.docs[0];
                let employee = doc.data();
                $('#featured_employee img').attr('src',employee.image);
                $('#featured_employee .name').text(employee.first_name+' '+employee.last_name);
            }
        });
        db.collection('headline')
        .onSnapshot(function(snapshot){
            if(snapshot.docs.length){
                $('#ticker_text').empty();
                snapshot.docs.forEach(function(doc){
                    $('#ticker_text').append('<span>'+doc.data().text+'</span> ');
                });
                runTicker();
            }
        });
    }

    firebase.initializeApp(config);
    db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });
    headerInit();
    init();



    // =====================  Clock functionality ===========================
    (function () {
      var clockElement = document.getElementById( "clock" );

      function updateClock ( clock ) {
        clock.innerHTML = new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
      }
      setInterval(function () {
          updateClock( clockElement );
      }, 60000);
      updateClock( clockElement );
    }());

    
    // ===================== Tracker Functionality ===========================
    $(document).on('click','.trackable',function(e){
        let el = $(this);
        let data = $(this).data();
        let tracker = db.collection('analytics').doc();
		tracker.set(data)
		.then(function(doc) {
			if(!doc.exists){
				main_doc = db.collection(collection).doc();
			}
		});
    });

	// =====================  Ticker functionality ===========================
	function runTicker() {
		var txt = $('#ticker_text');
        var spans = $('#ticker_text span').length * 90;
        var text_w = $(txt).width() + spans - 50;
        var time_to_scroll = Math.round(text_w / 40); 
		
		$(txt).css('transition', 'transform ' + time_to_scroll + 's');
		$(txt).css('transform', 'translateX(-100%)');
		resetTicker( txt, time_to_scroll );
		
	}

	function resetTicker( txt, time ) {
		var timer;
		timer = setTimeout(function(){
			$(txt).css('transition', 'none ');
			$(txt).css('transform', 'translateX(102%)');
			timer = setTimeout(function(){	
				runTicker();
			}, 1000);
		}, time * 1000 );
	}

	runTicker();



	// =====================  Slider functionality ===========================
	function runSlides( slide ) {
        $(slide).addClass('active_slide');
        slideTime = parseInt($(slide).attr('data-time')) * 60000;
        if ( !slideTime ) {
             slideTime = parseFloat($(slide).attr('data-time')) * 60000;
         }
 
        timer = setTimeout(function(){
                $(slide).removeClass('active_slide');
                if ( $(slide).next('.slide').length > 0 ) {
                     runSlides($(slide).next('.slide'));
                }else {
                    runSlides($($('.slide')[0]));
                }
        }, slideTime);
    }


    $(window).load(function(){
        if ( $('.slide').length > 1 ) {
            runSlides($($('.slide')[0]));
            $('.move_slide').css('display', 'flex');
        }else {
            $($('.slide')[0]).addClass('active_slide');
        }
    });


    $('.move_slide').click(function(){
        window.clearTimeout(timer);
        var slide = $('.active_slide');
        var prevSlide = $(slide).prev('.slide');
        var nextSlide = $(slide).next('.slide');
        $(slide).removeClass('active_slide');

        if ( $(this).hasClass('slide-left') ) {
            slideLeft(slide, prevSlide);
        }else {
            slideRight(slide, nextSlide);
        }
    });

    function slideLeft(slide, prevSlide) {
        if ( prevSlide.length != 0 ) {
            runSlides(prevSlide);
        }else {
            let slide = $($('.slide')[$('.slide').length-1])
            runSlides(slide);
        }
    }

    function slideRight(slide, nextSlide) {
        slideTime = parseInt($(nextSlide).attr('data-time')) * 60000;
        if ( nextSlide.length != 0 ) {
            runSlides(nextSlide);
        }else {
            runSlides($($('.slide')[0]));
        }
    }


    $(document).on('click', '.active_slide', function(){
        window.clearTimeout(timer);
        timer = setTimeout(function(){
            runSlides($(this));
        }, 60000);
    });



	// =====================  Link navigation ===========================
	$('.links .button').click(function(){
        $('.links .button').css('pointer-events', 'none');
		$('.load').addClass('start_load');
        $('.links .button').removeClass('red');
        $(this).addClass('red');
		var id = $(this).attr('id');
		$.ajax({
            url:id,
                type:'GET',
                success: function(data){
                    var timer;
                    $('.jQKeyboardContainer').hide();
                    timer = setTimeout(function(){
                        $('.content').fadeOut(500);
                        timer = setTimeout(function(){
                            if(id != '/home/survey'){
			    			$('.links .button').css('pointer-events', 'all');
	                    		$('.load').removeClass('start_load');
                            }
                            $('.content').html($(data).filter('.content').html()).fadeIn();
                            init();
                        }, 2000);
                    }, 100);
                }
        });
	});



	// =====================  Trivia Action ===========================



    // =====================  TIMEOUT  ===========================
    var initial = null;

    function invoke() {
        if ( !$('.content .home_page').is(':visible') ) {
            initial = window.setTimeout(               
                function() {
                    setTimeout(function(){
                        window.location = '/';
                  }, 500);
              }, 240000);
        }else {
            initial = setTimeout(function(){
                window.location = '/';
            }, 240000);
        }
    }


    invoke();

    $('body').on('click mousemove', function(){
        window.clearTimeout(initial);
        invoke();
        // invoke2();
    });

    document.addEventListener('contextmenu', event => event.preventDefault());


      


});