<?php

namespace Model;

class Package extends \Emagid\Core\Model {
    static $tablename = "public.package";

    public static $fields  =  [
    	'provider_id',
    	'title',
    	'description_1',
    	'description_2',
    	'description_3',
		'image'
    ];    

}