<?php

namespace Model;

class App_Download extends \Emagid\Core\Model {
    static $tablename = "public.app_donwnload_counts";

    public static $fields  =  [
        'phone',
        'app_store',
        'download_through',
    ];
}