
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>main.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type='text/javascript'>
<? if(SITE_ENV) {?>
var config = {
    apiKey: "AIzaSyB1IcigVOKi6cldhI_9yC30S6bIXXA5Los",
    authDomain: "reddoorspa-ab813.firebaseapp.com",
    databaseURL: "https://reddoorspa-ab813.firebaseio.com",
    projectId: "reddoorspa-ab813",
    storageBucket: "reddoorspa-ab813.appspot.com",
    messagingSenderId: "814931123452"
};
<? } else {?>
var config = {
    apiKey: "AIzaSyCEw6Z9bU_2UGstVVN-4D9FDRsikgUG1xE",
    authDomain: "reddoordev-234cc.firebaseapp.com",
    databaseURL: "https://reddoordev-234cc.firebaseio.com",
    projectId: "reddoordev-234cc",
    storageBucket: "reddoordev-234cc.appspot.com",
    messagingSenderId: "631395193427"
};
<? } ?>
</script>
<script src="<?=FRONT_JS?>main.js"></script> 



<script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-functions.js"></script>
<script src="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.css" />
