<header>
	<div class='logo'>
		<a href="/"><img src="<?= FRONT_ASSETS ?>img/logo.jpg"></a>
	</div>

	<div class='employee_m' id="featured_employee" >
		<p>Employee of the month</p>
		<img class='photo' src="<?= FRONT_ASSETS ?>img/emp1.jpg">
		<p class='name'>Firstname Lastname</p>
	</div>

	<div class='links'>
		<button id='/home/social_media/' data-btntitle='Social Media' data-currpage='Home Page' data-pagedestination='Social Media' class='trackable button'>SOCiaL MEDIA</button>
		<button id='/home/culture/' data-btntitle='Get in the Know'' data-currpage='Home Page' data-pagedestination='Get in the Know'' class='trackable button'>GEt in the know</button>
		<button id='/home/promotions/' data-btntitle='Promotions' data-currpage='Home Page' data-pagedestination='Promotions' class='trackable button'>Promotions</button>
		<button id='/home/survey/' data-btntitle='Survey' data-currpage='Home Page' data-pagedestination='Survey' class='trackable button'>Survey</button>
	</div>

	<div class='weather'>
		<span id="clock"></span>
		<div id="Oplao" data-id="31971" data-wd="300px" data-hg="250px" data-l="en" data-c="5128581" data-t="F" data-w="m/s" data-p="hPa" data-wg="widget_13" class="300 250"></div><script type="text/javascript" id="informer" charset="UTF-8" src="https://oplao.com/js/informer.js"></script>
	</div>
</header>

<section class='news'>
	<div class='bar'>
		<p>NEWS</p>
		<p id="ticker_text"></p>
	</div>
</section>

<div class='load'>
	<img class='load_logo' src="<?= FRONT_ASSETS ?>img/logo_load.png">
	<img class='loader' src="<?= FRONT_ASSETS ?>img/loader.png">
</div>